<%
'Arquivo criado para criar funções que seram usadas dentro do sistema
'################################################################################################'
'########################### INICIO FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################'
Function validaEmail(ByVal strEmail)
	Dim regEx
	Dim ResultadoHum
	Dim ResultadoDois 
	Dim ResultadoTres
	Set regEx = New RegExp          
	regEx.IgnoreCase = True        
	regEx.Global = True             

	regEx.Pattern	= "[^@\-\.\w]|^[_@\.\-]|[@\.]{2}|(@)[^@]*\1"
	ResultadoHum	= RegEx.Test(strEmail)

	regEx.Pattern	= "@[\w\-]+\."        
	ResultadoDois	= RegEx.Test(strEmail)

	regEx.Pattern	= "\.[a-zA-Z]{2,3}$"  
	ResultadoTres	= RegEx.Test(strEmail)
	Set regEx = Nothing

	If Not (ResultadoHum) And ResultadoDois And ResultadoTres Then
		validaEmail = True
	Else
		validaEmail = False
	End If
End Function


function LimparTexto(str)
	str = trim(str)
	str = replace(str,"=","")
	str = replace(str,"'","")
	'str = replace(str,"""""","")
	str = replace(str,Chr(34),"")
	str = replace(str,Chr(147),"")
	str = replace(str,"”","")
	str = replace(str,"“","")
	str = replace(str," or ","")
	str = replace(str," and ","")
	str = replace(str,"(","")
	str = replace(str,")","")
	str = replace(str,"<","[")
	str = replace(str,">","]")
	str = replace(str,"update","")
	str = replace(str,"-shutdown","")
	str = replace(str,"--","")
	str = replace(str,"'","")
	str = replace(str,"#","")
	str = replace(str,"$","")
	str = replace(str,"%","")
	str = replace(str,"¨","")
	str = replace(str,"&","")
	str = replace(str,"'or'1'='1'","")
	str = replace(str,"--","")
	str = replace(str,"insert","")
	str = replace(str,"drop","")
	str = replace(str,"delet","")
	str = replace(str,"xp_","")
	str = replace(str,"select","")
	str = replace(str,"*","")
	LimparTexto = str
end function

Function get_query(TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		'ABRE CONEXAO COM O  BANCO DE DADOS'
		Connect.Open dB

		'Instancia o objeto do recordset
		Set TEMP = Server.CreateObject("ADODB.Recordset")

		'VERIFICA SE PARAM É FALSE'
		if PARAM = false Then
			PARAM = ""
		end if

		'VERIFICA SE WHERE É FALSE'
		if WHERE = false then
			WHERE = ""
		End if

		'VERIFICA SE INNER É FALSE'
		if INNER = false then
			INNER = ""
		End if

		'VERIFICA SE GROUP_BY É FALSE'
		if GROUP_BY = false then
			GROUP_BY = ""
		End if

		'VERIFICA SE HAVING É FALSE'
		if HAVING = false then
			HAVING = ""
		End if

		'VERIFICA SE ORDER_BY É FALSE'
		if ORDER_BY = false then
			ORDER_BY = ""
		End if

		'MONTANDO QUERY'
		QUERY = "SELECT * " & PARAM & " FROM " & TABELA & INNER & WHERE & GROUP_BY & HAVING & ORDER_BY

		'VERIFICA SE DEBUG ESTÁ TRUE'
		IF DEBUG = true then
			Response.write(QUERY)
			Response.End
		end if

		'EXECUTA QUERY'
		TEMP.Open QUERY, Connect, 3, 1
		
		'ENCERRANDO OBJETO ADODB.Recordset'
		Set TEMP.ActiveConnection = Nothing

		'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing	

		'RECEBENDO E RETORNANDO A CONSULTA'
		Set get_query = TEMP


	On Error Goto 0	
end function

function update(TABELA, PARAM, WHERE, DEBUG)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection ANTES DE ENTRAR NESSA FUNCTION'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		Connect.Open db

		'MONTANDO QUERY'
		QUERY = "UPDATE " & TABELA & PARAM & WHERE

		'VERIFICA SE DEBUG ESTÁ TRUE'
		IF DEBUG = true then
			Response.write(QUERY)
			Response.End
		end if

		'EXECUTA QUERY'
		Connect.execute(QUERY)

		'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing
	On Error Goto 0	
end function

function delete(TABELA, WHERE, DEBUG)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection ANTES DE ENTRAR NESSA FUNCTION'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		Connect.Open db

		'MONTANDO QUERY'
		QUERY = "DELETE FROM " & TABELA & WHERE

		'VERIFICA SE DEBUG ESTÁ TRUE'
		IF DEBUG = true then
			Response.write(QUERY)
			Response.End
		end if

		'EXECUTA QUERY'
		Connect.execute(QUERY)

		'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing
	On Error Goto 0	
end function

function insert(TABELA, PARAM, DEBUG)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		Connect.Open db

		'MONTANDO QUERY'
		QUERY = "INSERT INTO "& TABELA & PARAM

		'VERIFICA SE DEBUG ESTÁ TRUE'
		IF DEBUG = true then
			Response.write(QUERY)
			Response.End
		end if

		'EXECUTA QUERY'
		Connect.execute QUERY, RecordsAffected

		codigo_inserido = Connect.Execute( "SELECT @@IDENTITY FROM "&TABELA)(0).Value

	    'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing

		'RECEBENDO E RETORNANDO A CONSULTA'
	    insert = codigo_inserido

    On Error Goto 0	
end function

Function FormataData(Data, tipo)
	If Data <> "" Then 

		ANO = DatePart("yyyy", Data)
		MES = Right("0" & DatePart("m", Data),2)
		DIA = Right("0" & DatePart("d", Data),2)

		Select Case tipo
			Case 1 'CONVERTE DD/MM/YYYY PARA YYYY/MM/DD'
				FormataData = ANO &  "-" & MES & "-" &  DIA
			Case 2 'CONVERTE YYYY-MM-DD PARA DD/MM/YYYY'
				FormataData = DIA & "/" & MES & "/" & ANO
		End Select
   	end if
   	'FINAL VERIFICA DATA DIFERENTE DE VAZIO'

End Function

Function CriarPasta(ByVal pCaminho, ByVal pNomePasta) 
	On Error Resume Next
		Dim caminho
		Dim pasta 
		Dim objFS
        
        caminho = pCaminho
        pasta = pNomePasta

        Set objFS = Server.CreateObject("Scripting.FileSystemObject")
        
        If Not objFS.FolderExists( caminho & pasta)  Then
        	objFS.CreateFolder( caminho & pasta )
        	criarPasta = true
        	Set objFS = Nothing
        Else
        	criarPasta = false
        	Set objFS = Nothing	
        End If
        
	On Error Goto 0	
End Function

Function sendEmail(remetente,remetente_nome,destino,copia,assunto,corpo)
	
	On Error Resume Next

		Set Mail = Server.CreateObject("Persits.MailSender")

		Mail.Host = "smtp.kbrtec.com.br"
		Mail.Port = 587
		Mail.Username = "weslley.santo@kbrtec.com.br"
		Mail.Password = "wesley2015so"

		'Configura o e-mail do remetente da mensagem que OBRIGATORIAMENTE deve ser um e-mail do seu próprio domínio
		if remetente <> false then
			Mail.From = remetente
		Else
			Mail.From = "no-reply@kbrtec.com.br"
		End if
		'Configura o Nome do remetente da mensagem

		Mail.FromName = remetente_nome

		'Configura os destinatários da mensagem
		Mail.AddAddress destino
		
		IF copia <> false then
			Mail.AddBcc copia
		end if
		
		'Configura o Assunto da mensagem enviada
		Mail.Subject = assunto

		'Configura o conteúdo da Mensagem
		Mail.Body = corpo
		
		Mail.IsHTML = True
		Mail.Send

		If err <> 0 Then
			sendEmail = false
			Set Mail = Nothing
		Else
			sendEmail = true
			Set Mail = Nothing
		End If
				
	On Error Goto 0
End Function

Function renomear(ByVal arquivo, ByVal novoNome)
	Set objFS = Server.CreateObject("Scripting.FileSystemObject")
	extensao = "." & objFS.GetExtensionName(arquivo)
	renomear = novoNome & extensao
	Set objFS = Nothing
End Function

sub crop(x1_ ,y1_ ,width_ ,height_ , caminho, nome_arquivo, DEBUG)
	On Error Resume Next
	
	If DEBUG = true then
		Response.write "x1: " & x1
		Response.write "<br/>"
		Response.write "y1: " & y1
		Response.write "<br/>"
		Response.write "width: " & width
		Response.write "<br/>"
		Response.write "height: " & height
		Response.write "<br/>"
		Response.write "caminho: " & caminho
		Response.write "<br/>"
		Response.write "nome_arquivo: " & nome_arquivo
		Response.End
	end if

	Set Jpeg = Server.CreateObject("Persits.Jpeg")

	x1 = cint(x1_)
	y1 = cint(y1_)
	width = cint(width_)
	height = cint(height_)

	Jpeg.Open caminho &"\" & nome_arquivo
	Jpeg.Canvas.Brush.Color = &HFFFFFF
	Jpeg.Crop x1, y1, x1 + width , y1 + height

	Jpeg.Save caminho & "\crop_"&nome_arquivo
			
	Set Jpeg = Nothing

	On Error Goto 0	
end sub

Sub redimensionaImagem(altura, largura, caminho, caminho_novo, nome_arquivo, DEBUG)

	If DEBUG = true then
		Response.write "altura: " & altura
		Response.write "<br/>"
		Response.write "largura: " & largura
		Response.write "<br/>"
		Response.write "caminho: " & caminho
		Response.write "<br/>"
		Response.write "caminho_novo: " & caminho_novo
		Response.write "<br/>"
		Response.write "nome_arquivo: " & nome_arquivo
		Response.End
	end if

	If IsNumeric(largura) And IsNumeric(altura) Then
		Set Jpeg = Server.CreateObject("Persits.Jpeg")
		
		width_img  = largura
		height_img = altura
		
		Jpeg.Open caminho & "crop_"&nome_arquivo
		
		If width_img <> Cint(Jpeg.OriginalWidth) and height_img <> Cint(Jpeg.OriginalHeight) Then 	
			If Cint(Jpeg.OriginalHeight) > height_img Or Cint(Jpeg.OriginalWidth) > width_img Then
				If Cint(Jpeg.OriginalHeight) > Cint(Jpeg.OriginalWidth) Then
					Jpeg.Height  = height_img
					Jpeg.Width   = cint((Jpeg.OriginalWidth * Jpeg.Height)/Jpeg.OriginalHeight)
				Else
					Jpeg.Width   = width_img
					Jpeg.Height  = cint((Jpeg.OriginalHeight * Jpeg.Width)/Jpeg.OriginalWidth)
				End If
			End if
			
			Jpeg.Quality = 100
			'Jpeg.Sharpen 1,150
			'Response.write("caminho_novo: " & caminho_novo)

			Jpeg.Save caminho_novo & nome_arquivo
			
			Set Jpeg = Nothing
		Else
			'Declara a variável a ser usada
			Dim fso

			'Cria a instância com o objeto ObjFile
			Set fso = CreateObject("Scripting.FileSystemObject")

			'COPIA o arquivo da pasta atual para a pasta informada
			fso.CopyFile caminho & nome_arquivo, caminho_novo
			'Move o arquivo da pasta atual para a pasta informada
			'fso.MoveFile caminho & nome_arquivo, caminho_novo & nome_novo

			'Elimina variável da memória 
			Set fso = Nothing
		End If	
			
	End If
End Sub

Function delete_imagem(file)
	
	dim fs
	set fs = Server.CreateObject("Scripting.FileSystemObject")

	if fs.FileExists(file) then
		fs.DeleteFile(file)
	end if

	set fs=nothing
End Function


'################################################################################################'
'########################### FINAL FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################'

'################################################################################################'
'################################## INICIO FUNÇÕES DO SISTEMA ####################################'
'################################################################################################'

'################ INICIO CATEGORIA'
Function get_categorias
	On Error Resume Next

	TABELA = "tb_categoria"
	WHERE = " WHERE ic_status = '1'" 
	ORDER_BY = " ORDER BY nm_categoria ASC"
								'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   set get_categorias =  get_query(TABELA, false, WHERE, false, false, false, ORDER_BY, false)
		
	On Error Goto 0	
end function

Function verificaCategoria(nm_categoria, id_categoria)
	On Error Resume Next

	TABELA = "tb_categoria"
	WHERE = " WHERE nm_categoria = '"&nm_categoria&"' AND ic_status = '1'"
	if id_categoria <> false then
		WHERE = WHERE & " AND id_categoria <> "&id_categoria		
	end if

									'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   set verificaCategoria =  get_query(TABELA, FALSE, WHERE, FALSE, false, false, ORDER_BY, FALSE)
		
	On Error Goto 0	
end function

Function valicao_categoria(dados)
	On Error Resume Next

	if IsEmpty(dados("nm_categoria")) then
		retorno = "{""res"":""error"", ""msg"":""Preencha o nome da categoria!""}"    	
		valicao_categoria = Response.write(retorno)
    	response.end
	elseif dados("id_categoria") = "0" then
		set verifica_categoria = verificaCategoria(dados("nm_categoria"), false)
    	if not verifica_categoria.EOF Then
			retorno = "{""res"":""error"", ""msg"":""Já existe uma categoria com esse nome //SEM ID!""}"    	
			valicao_categoria = Response.write(retorno)
    		response.end
		end if
	elseif dados("id_categoria") <> "0" then
		set verifica_categoria = verificaCategoria(dados("nm_categoria"), dados("id_categoria"))
		if not verifica_categoria.EOF Then
			retorno = "{""res"":""error"", ""msg"":""Já existe uma categoria com esse nome //COM ID!""}"    	
			valicao_categoria = Response.write(retorno)
    		response.end
		end if
	end if 
   			
	On Error Goto 0	
end function

'FINAL CATEGORIA #########################'

'################ INICIO PERFIL'
Function get_perfil(find, HAVING, id)
	On Error Resume Next

	TABELA = "tb_tags tg, tb_categoria tc, tb_perfil tp"
	PARAM = " , tp.id_perfil, tp.nm_perfil, tp.nm_email, tp.dt_nascimento, tp.ds_foto, GROUP_CONCAT(distinct(tc.nm_categoria)) as categorias, tt.nm_tipo, tst.nm_subtipo, GROUP_CONCAT(distinct(tg.nm_tags)) as tags"
	WHERE =" WHERE  tpc.id_categoria = tc.id_categoria AND tpt.id_tags = tg.id_tags AND tp.ic_status = '1' AND tc.ic_status = '1' AND tg.ic_status = '1' AND tt.ic_status = '1' AND tst.ic_status = '1' AND tp.ic_linguagem = 'A'"
	INNER = " INNER JOIN tb_tipo tt ON tp.id_tipo = tt.id_tipo INNER JOIN tb_subtipo tst ON tp.id_subtipo = tst.id_subtipo INNER JOIN tb_perfil_categoria tpc ON tp.id_perfil = tpc.id_perfil INNER JOIN tb_perfil_tags tpt ON tp.id_perfil = tpt.id_perfil" 
	GROUP_BY = " GROUP BY tp.id_perfil"

	if find <> false then
		WHERE = WHERE & find
	end if

	if id <> false then
		WHERE = WHERE & id
	end if	

								'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   set get_perfil =  get_query(TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, false, FALSE)

	On Error Goto 0	
end function

Function verificaEmail(nm_email, id_perfil)
	On Error Resume Next

	TABELA = "tb_perfil"
	WHERE = " WHERE nm_email = '"&nm_email&"'"
	
	if id_perfil <> false then
		WHERE = WHERE & " AND id_perfil <> "&id_perfil		
	end if

								'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   set verificaEmail =  get_query(TABELA, false, WHERE, FALSE, false, false, ORDER_BY, false)

	On Error Goto 0	
end function

Function valicao_perfil(dados)
	On Error Resume Next


	if IsDate(dados("dt_nascimento")) = false then
	    retorno = "{""res"":""error"", ""msg"":""Data inválida!""}"
	    valicao_perfil = Response.write(retorno)
    	response.end
    elseif validaEmail(dados("email")) = false then 
    	retorno = "{""res"":""error"", ""msg"":""Preencha o E-mail corretamente!""}"
	    valicao_perfil = Response.write(retorno)
	    Response.end
	elseif dados("id_perfil") = "0" Then
		set verifica_email = verificaEmail(dados("email"), false)
		if not verifica_email.EOF Then
			retorno = "{""res"":""error_email"", ""msg"":""Já existe um e-mail cadastrado com esse endereço!""}"
			valicao_perfil = Response.write(retorno)
		    Response.end
		end if
	elseif dados("id_perfil") <> "0" Then
		set verifica_email = verificaEmail(dados("email"), dados("id_perfil"))
		if not verifica_email.EOF Then
			retorno = "{""res"":""error_email"", ""msg"":""Já existe um e-mail cadastrado com esse endereço!""}"
			valicao_perfil = Response.write(retorno)
		    Response.end
		end if
    end if
   			
	On Error Goto 0	
end function
'FINAL PERFIL #########################'

'######################### INICIO PERFIL CATEGORIA'
Function insert_perfil_categoria(id_perfil, id_categoria)
	On Error Resume Next

	TABELA = "tb_perfil_categoria "

	call delete(TABELA, "WHERE id_perfil = "&id_perfil, false)

	id_categoria_slipt = Split(id_categoria,",")

	for each x in id_categoria_slipt
		PARAM = "(id_perfil, id_categoria) VALUES("&id_perfil&", "&x&")"

    	call insert(TABELA, PARAM, false)
	next

	On Error Goto 0	
end function

Function get_perfil_categoria(id_perfil)
	On Error Resume Next

	TABELA = "tb_perfil_categoria tpc"
	WHERE = " WHERE tpc.id_perfil = " & id_perfil

									'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   set get_perfil_categoria =  get_query(TABELA, false, WHERE, false, false, false, false, false)

	On Error Goto 0	
end function

'FINAL PERFIL CATEGORIA #########################'


'######################### INICIO PERFIL TAGS'
Function insert_perfil_tags(id_perfil, id_tags, DEBUG)
	On Error Resume Next

	IF DEBUG = TRUE Then
		response.write("id_perfil: " & id_perfil)
		response.write(" | id_tags: " & id_tags)
		response.end
	end if

	TABELA_PERFIL_TAGS = "tb_perfil_tags "
	TABELA_TAGS = "tb_tags"
	
	'DELETAR TODAS AS TAGS JÁ CADASTRADAS DO PERFIL'
	call delete(TABELA_PERFIL_TAGS, "WHERE id_perfil = " & id_perfil, false)

	tags_slipt = Split(id_tags,",")

	'VERFICAR SE EXISTE A TAGS'
	for each nm_tag in tags_slipt

		WHERE = " WHERE nm_tags = '" & nm_tag & "'"

    	 							'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   		set retorno_query =  get_query(TABELA_TAGS, false, WHERE, false, false, false, false, false)
   		
   		If not retorno_query.EOF Then 'VERIFICA SE TRUE 
         	Do While Not retorno_query.EOF
				get_id_tag = retorno_query.Fields("id_tags").Value

				PARAM = "(id_perfil, id_tags) VALUES("&id_perfil&", "&get_id_tag&")"
   				'INSERIR AS TAGS QUE JÁ EXISTEM NA TABELA TB_TAGS EM TB_PERFIL_TAGS'
   							'TABELA, PARAM, DEBUG'	
				call insert(TABELA_PERFIL_TAGS, PARAM, false)
				'Response.WRITE("INSERINDO: " & get_id_tag & " IN " & TABELA_PERFIL_TAGS & " :::: ")
				retorno_query.MoveNext
           	Loop
			'FINAL LOOP WHILE'
            retorno_query.close
        else 'SE NÃO ENCONTROU UM ID DE RETORNO, INSERE EM TB_TAGS, PEGA ID E INSERE EM TB_PERFIL_TAGS'
        	PARAM = "(nm_tags) VALUES('"&nm_tag&"')"
			'INSERINDO NOVAS TAGS E RETORNANDO O ULTIMO ID'        							
        							'TABELA, PARAM, DEBUG'
        	last_id_tag = insert(TABELA_TAGS, PARAM, false)
        	'Response.write("last_id_tag: " & last_id_tag)
        	'INSERIR EM PERFIL_TAGS AS NOVAS TAGS ADICIONADAS'
    		PARAM = "(id_perfil, id_tags) VALUES("&id_perfil&", "&last_id_tag&")"
   				'INSERIR AS TAGS QUE JÁ EXISTEM NA TABELA TB_TAGS EM TB_PERFIL_TAGS'
						'TABELA, PARAM, DEBUG'	
			call insert(TABELA_PERFIL_TAGS, PARAM, false)
        end if
	next	

	On Error Goto 0	
end function

Function get_perfil_tags(id_perfil)
	On Error Resume Next

	TABELA = "tb_perfil_tags tpt"
	WHERE = " WHERE tpt.id_perfil = " & id_perfil

									'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   set get_perfil_tags =  get_query(TABELA, false, WHERE, false, false, false, false, false)

	On Error Goto 0	
end function

'FINAL PERFIL TAGS #########################'

'################ INICIO SUBCATEGORIA'
Function get_subcategorias
	On Error Resume Next

	TABELA = "tb_subcategoria sc"
	WHERE = " WHERE sc.ic_status = '1' AND c.ic_status = '1'"
	INNER = " INNER JOIN tb_categoria c ON sc.id_categoria = c.id_categoria"
	ORDER_BY = " ORDER BY nm_subcategoria, nm_categoria ASC"
									'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   set get_subcategorias =  get_query(TABELA, false, WHERE, INNER, false, false, ORDER_BY, false)
		
	On Error Goto 0	
end function

Function valicao_subcategoria(dados)
	On Error Resume Next

	if IsEmpty(dados("nm_subcategoria")) then
		valicao_subcategoria = Response.write("Preencha o nome da subcategoria")
		Response.end
	elseif IsEmpty(dados("id_categoria")) then
		valicao_subcategoria = Response.write("Selecione uma categoria")
		response.end
	end if   
   			
	On Error Goto 0	
end function

Function verificaSubcategoria(nm_subcategoria, id_categoria, id_subcategoria)
	On Error Resume Next

	TABELA = "tb_subcategoria"
	WHERE = " WHERE nm_subcategoria = '"&nm_subcategoria&"' AND id_categoria = "& id_categoria & " AND ic_status = '1'"
	
	if id_subcategoria <> false then
		WHERE = WHERE & " AND id_subcategoria <> " & id_subcategoria		
	end if

									'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   set verificaSubcategoria =  get_query(TABELA, false, WHERE, FALSE, false, false, ORDER_BY, false)
		
	On Error Goto 0	
end function
'FINAL SUBCATEGORIA #########################'

'######################### INICIO TIPOS '
Function get_tipos
	On Error Resume Next

	TABELA = "tb_tipo "
	WHERE = " WHERE ic_status = '1' "
	ORDER_BY = " ORDER BY nm_tipo ASC"
							'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   set get_tipos =  get_query(TABELA, false, WHERE, false, false, false, ORDER_BY, FALSE)
		
	On Error Goto 0	
end function
'FINAL TIPOS #########################'

'######################### INICIO TAGS '
Function get_tags_by_perfil
	On Error Resume Next

	TABELA = "tb_tags tt"
	WHERE = " WHERE ic_status = '1' "
	ORDER_BY = " ORDER BY nm_tags ASC"
										'TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
   	set get_tags_by_perfil =  get_query(TABELA, false, WHERE, false, false, false, ORDER_BY, FALSE)
		
	On Error Goto 0	
end function
'FINAL TAGS #########################'

'################################################################################################'
'################################## FINAL FUNÇÕES DO SISTEMA ####################################'
'################################################################################################'
%>