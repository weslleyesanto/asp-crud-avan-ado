<!--#include file="include\config.asp"-->
<!--#include file="include\md5.asp"-->
<%
url = "default.asp"

if  Request.ServerVariables("REQUEST_METHOD") = "POST" then
    
    Response.ContentType = "application/json"
	
    'Instancia o componente
    SET SaFileUp = Server.CreateObject("SoftArtisans.FileUp")
    SaFileUp.CodePage = 65001
    
    valicao_perfil(SaFileUp.Form)


    'response.write("SEM: " & SaFileUp.Form("nm_perfil"))
    'response.write(" | COM: " & LimparTexto(SaFileUp.Form("nm_perfil")))
    'response.end

	nm_perfil = LimparTexto(SaFileUp.Form("nm_perfil"))
    email = LimparTexto(SaFileUp.Form("email"))
    dt_nascimento = FormataData(LimparTexto(SaFileUp.Form("dt_nascimento")), 1)
	
	foto = LimparTexto(SaFileUp.Form("foto"))
	x1 = LimparTexto(SaFileUp.Form("x1"))
	y1 = LimparTexto(SaFileUp.Form("y1"))
	width = LimparTexto(SaFileUp.Form("width"))
	height = LimparTexto(SaFileUp.Form("height"))
	ds_foto = LimparTexto(SaFileUp.Form("ds_foto"))

	id_categoria = LimparTexto(SaFileUp.Form("id_categoria"))
	id_tipo = LimparTexto(SaFileUp.Form("id_tipo"))
	id_subtipo = LimparTexto(SaFileUp.Form("id_subtipo"))
	tags = LimparTexto(SaFileUp.Form("nm_tags"))

    id_perfil = LimparTexto(SaFileUp.Form("id_perfil"))
	id_categoria = LimparTexto(SaFileUp.Form("id_categoria"))
    
    modulo = "Perfil"
    TABELA = "tb_perfil"

    'CAMINHA DO ARQUIVO
	file_path  = "assets/imagem/perfil/"

	if ds_foto = "" then 'INSERIR NOVO REGISTRO'
		'VERIFICA SE FOTO ESTÁ IGUAL A VAZIO'
		if foto = "" then
			response.write "<center>Por favor, indique um arquivo para upload.</center><br>"
			response.end
		else

			foto = LimparTexto(SaFileUp.Form("foto").userFileName)
			nova_foto = MD5(now) &".jpg"

			'PREPARANDO PARAMETRO PARA INSERIR NO PERFIL
			PARAM = "(nm_email, nm_perfil, dt_nascimento, ds_foto, id_tipo, id_subtipo, ic_linguagem) VALUES ('" & email &"', '" & nm_perfil &"', '" &dt_nascimento &"', '" & nova_foto &"', " & id_tipo &", " & id_subtipo & ", 'A')"
			
			id_perfil = insert(TABELA, PARAM, false)

			If id_perfil <> "" Then 'SE INSERIU RETORNO VALOR DIFERENTE DE VAZIO

				'CHAMANDO FUNÇÃO PARA INSERIR EM PERFIL_CATEGORIA'
				call insert_perfil_categoria(id_perfil, id_categoria)
				'CHAMANDO FUNÇÃO PARA INSERIR EM PERFIL_TAGS'
				call insert_perfil_tags(id_perfil, tags, false)

				'CRIAR A PASTA COM O ID DO USUÁRIO
				pasta_id = criarPasta(ABSOLUTO_IMAGEM_PERFIL, id_perfil)

			    'VERIFICA SE CRIOU A PASTA COM ID DO PERFIL'
				IF pasta_id = true Then 'CRIOU A PASTA COM O ID'
					
					'#INICIO UPLOAD DE IMAGEM PARA O SERVIDOR#'
					'CAMINHO RELATIVO NO SERVIDOR'
					SaFileUp.Path = Server.MapPath(file_path & id_perfil)
				    'SALVA IMAGEM'
				    SaFileUp.SaveAs nova_foto 
				    Set SaFileUp = Nothing 
				    '#FINAL UPLOAD DE IMAGEM PARA O SERVIDOR#'

		    		'CORTAR IMAGEM						x1,y1,width,height, caminho, nome_arquivo, DEBUG)
			    	call crop(x1, y1, width, height, ABSOLUTO_IMAGEM_PERFIL & id_perfil, nova_foto, false)

					ABSOLUTO_THUMB = ABSOLUTO_IMAGEM_PERFIL & id_perfil
					
					'CRIANDO A PASTA THUMN DENTRO DA PASTA COM ID DO PERFIL'
					pasta_thumb = criarPasta(ABSOLUTO_THUMB, "\thumb\")					
					'VERIFICA SE CRIAR OU PASTA THUMB'
					IF pasta_thumb = false then 'SE NÃO CRIOU RETORNO O ERRO'
						retorno = "{""res"":""error"", ""msg"":""Problema ao criar pasta do THUMB!""}"
					Else
						THUMB = ABSOLUTO_THUMB & "\thumb\"
						'REDIMENCIONA IMAGEM'
						call redimensionaImagem(90, 90, ABSOLUTO_IMAGEM_PERFIL & id_perfil & "\", THUMB, nova_foto, false)

						'#INICIO ENVIO DE E-MAIL#'
	    				nome_remetente = "KBRTEC"
	    				destino = email
	    				assunto = "Novo Cadastro"
	    				corpo = "Você foi cadastrado com sucesso!"

	    				envio = sendEmail(false,nome_remetente,destino, FALSE, assunto, corpo)
		    			'VERIFICA SE ENVIOU E-MAIL'
		    			if envio = false then 'SENAO ENVIOU EXIBE MENSAGEM DE ERRO'
		    				retorno = "{""res"":""error"", ""msg"":""Problema ao enviar e-mail!""}"
		    			end if
		    			'FINAL VERIFICA SE ENVIOU E-MAIL'
		    			'#FINAL ENVIO DE E-MAIL#'
					end if
					'FINAL VERIFICA SE CRIOU PASTA THUMB'
			    	
				else
					retorno = "{""res"":""error"", ""msg"":""Problema ao criar pasta do perfil!""}"
				End if
				'FINAL VERIFICA SE CRIOU A PASTA'

		    	retorno = "{""res"":""ok"", ""msg"":"""&modulo&" cadastrado com sucesso!"", ""url"": """&url&"""}"	
			else
				retorno = "{""res"":""error"", ""msg"":""Problema ao inserir novo "&modulo&"!""}"
			end if				
			'FINAL VERIFICA SE INSERIU NA TABELA CATEGORIA'
		end If
		'FINAL VERIFICA SE FOTO NÃO ESTÁ VAZIA'

	elseif ds_foto <> "" then
		
		if id_perfil <> "0" then 'SE ID FOR DIFERENTE DE ZERO É UMA ALTERAÇAO

    		'VERIFICA SE FOTO É DIFERENTE DE VAZIO'
			if foto <> "" then 'ALTERAR DADOS E FAZER UM NOVO UPLOAD'
				
				file = ABSOLUTO_IMAGEM_PERFIL & id_perfil & "\" & ds_foto
				file_crop = ABSOLUTO_IMAGEM_PERFIL & id_perfil & "\crop_" & ds_foto
				file_thumb = ABSOLUTO_IMAGEM_PERFIL & id_perfil & "\thumb\" & ds_foto

				'DELETA IMAGEM PRINCIPAL'
				call delete_imagem(file)

				'DELETA IMAGEM CROP'
				call delete_imagem(file_crop)

				'DELETA IMAGEM THUMB'
				call delete_imagem(file_thumb)

				foto = LimparTexto(SaFileUp.Form("foto").userFileName)
				nova_foto = MD5(now) &".jpg"

				'CAMINHO RELATIVO NO SERVIDOR'
				SaFileUp.Path = Server.MapPath(file_path & id_perfil)
			    SaFileUp.SaveAs nova_foto 'SALVA IMAGEM'
			    Set SaFileUp = Nothing 

			    'CORTAR IMAGEM						x1,y1,width,height, caminho, nome_arquivo, DEBUG)
		    	call crop(x1, y1, width, height, ABSOLUTO_IMAGEM_PERFIL & id_perfil, nova_foto, false)

			    'SALVAR O THUMB'
			    ABSOLUTO_THUMB = ABSOLUTO_IMAGEM_PERFIL & id_perfil
			    THUMB = ABSOLUTO_THUMB & "\thumb\"
	    								'altura, largura, caminho, caminho_novo, nome_arquivo, DEBUG
				call redimensionaImagem(90, 90, ABSOLUTO_IMAGEM_PERFIL & id_perfil & "\", THUMB, nova_foto, false)
			end if
			'FINAL VERIFICA SE FOTO É DIFERENTE DE VAZIO'

	    	'INICIO UPDATE
			PARAM = " SET nm_perfil = '"&nm_perfil&"', nm_email = '"&email&"', dt_nascimento = '"& dt_nascimento&"', id_tipo = "&id_tipo &", id_subtipo = "&id_subtipo 

			if foto <> "" then
				PARAM = PARAM & ", ds_foto = '"&nova_foto&"'"
			end if
			'FINAL VERIFICA SE A FOTO ESTÁ DIFERENTE DE VAZIO'

			WHERE = " WHERE id_perfil = " & id_perfil

						'TABELA, PARAM, WHERE, DEBUG'
			call update(TABELA, PARAM, WHERE, false)

			'CHAMANDO FUNÇÃO PARA INSERIR EM PERFIL_CATEGORIA'
			call insert_perfil_categoria(id_perfil, id_categoria)
			'CHAMANDO FUNÇÃO PARA INSERIR EM PERFIL_TAGS'
			call insert_perfil_tags(id_perfil, tags, false)

			url = "perfil_form.asp?q=s&acao=alterar&id="&id_perfil
		    retorno = "{""res"":""ok"", ""msg"":"""&modulo&" alterado com sucesso!"", ""url"": """&url&"""}"
	    	'FINAL VERIFICA SE EXISTE E-MAIL CADASTRADO'
		else
			Response.Redirect url	
		end if
		'FINAL VERIFICA SE ID DO PERFIL É DIFERENTE DE 0'
	end if
	'FINAL VERIFICA SE DS_FOTO É DIFERENTE DE VAZIO'
   	Response.write(retorno)

else
	Response.Redirect url
end if
'FINAL VERIFICA METODO DE REQUISIÇÃO IGUAL A POST'

%>