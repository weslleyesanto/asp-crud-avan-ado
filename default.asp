<!--#include file="include\config.asp"-->
<%                              'FIND, HAVING, ID'
Set retorno_query = get_perfil(false, false, false) 
%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Perfil</title>
    <!--#include file="include\css.asp"-->
</head>

<body>

    <div id="wrapper">

       <!--#include file="include\menu.asp"-->

       <div id="page-wrapper">


        <div class="container-fluid">
            <h1>Listagem Perfil</h1>
            <div id="alert" style="display:none;"> </div>
            
            <div id="">
                <a href="perfil_form.asp?q=s&acao=add" title="Adicionar Perfil">Adicionar</a>
                <br/><br/>
                Filtro: <input class="search selectable" type="search" placeholder="Pesquisar" data-column="all" id="pesquisar">
                
                Exportar: 
                <a href="exportar.asp?tipo=xls&pesquisar" title="Exportar Excel" id="exportar_excel">
                    <img src="assets/imagem/icone/icon-xls.png" alt="Exportar Excel" title="Exportar Excel">
                </a>
                <a href="exportar.asp?tipo=txt&pesquisar" title="Exportar Text" id="exportar_txt">
                <img src="assets/imagem/icone/icon-txt.png"alt="Exportar txt" title="Exportar txt"></button>
                </a>

                <table id="listaPerfil" class="tablesorter">
                    <%  If not retorno_query.EOF Then 'VERIFICA SE TRUE %>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumb</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Data de nascimento</th>
                            <th>Categoria</th>
                            <th>Tipo</th>
                            <th>Subtipo</th>
                            <th>Tags</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                     <%     
                     Do While Not retorno_query.EOF
                            id_perfil = retorno_query.Fields("id_perfil").Value
                            nm_perfil = retorno_query.Fields("nm_perfil").Value
                            nm_email = retorno_query.Fields("nm_email").Value
                            dt_nascimento = retorno_query.Fields("dt_nascimento").Value
                            ds_foto = retorno_query.Fields("ds_foto").Value
                            nm_categoria = retorno_query.Fields("categorias").Value
                            nm_tipo = retorno_query.Fields("nm_tipo").Value
                            nm_subtipo = retorno_query.Fields("nm_subtipo").Value
                            tags = retorno_query.Fields("tags").Value
                           %>
                            <tr data-id="<%=id_perfil%>" modulo="perfil" page="default">
                                <td><%=id_perfil%></td>
                                <td>
                        <img src="<%=RELATIVO_IMAGEM_PERFIL&id_perfil&pasta_thumb&ds_foto%>" alt="<%=nm_perfil%>" title="<%=nm_perfil%>" id="caminho_foto" />
                                </td>
                                <td><%=nm_perfil%></td>
                                <td><%=nm_email%></td>
                                <td><%=dt_nascimento%></td>
                                <td>
                                <% 
                                    if isnull(nm_categoria) Then
                                        response.write("Sem categoria")
                                    else
                                        response.write(nm_categoria)
                                    end if
                                %>
                                </td>
                                <td><%=nm_tipo%></td>
                                <td><%=nm_subtipo%></td>
                                <td><%=tags%></td>
                                <td id="acoes">
                                    <a href="perfil_form.asp?q=s&acao=alterar&id=<%=id_perfil%>" title="Alterar" class="alterar">
                                        Alterar
                                    </a>
                                    <a href="perfil_form.asp?q=s&acao=visualizar&id=<%=id_perfil%>" title="Visualizar">Visualizar</a>
                                    <a href="#" title="Excluir" class="excluir">Excluir</a>
                                    <a href="#" title="Notificar" class="notificar">Notificar</a>
                                </td>
                            </tr>
                            <%  
                            retorno_query.MoveNext
                            Loop
                            retorno_query.close
                            %>
                        </tbody>
                    </table>
                    <% else %>
                    <tr>
                        <td colspan="6">Nenhum registro encontrado!</td>
                    </tr>
                    <%
                    end if
                    %>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!--#include file="include\js.asp"-->
    <script src="assets/js/perfil.js"></script>
</body>

</html>
