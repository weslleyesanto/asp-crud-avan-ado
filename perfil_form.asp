<!--#include file="include\config.asp"-->
<%
REDIRECIONA = "default.asp"

Set categorias = get_categorias
Set tipos = get_tipos
Set tags = get_tags_by_perfil

selecionado = ""
visualizar = false
pasta_thumb = "/thumb/"

If Request.QueryString("q") = "s" Then 
    If Request.QueryString("acao") = "add" Then
        title = "Adicionar Perfil"
        nm_perfil = ""
        email = ""
        dt_nascimento = ""
        ds_foto = ""
        id_categoria = ""
        id_tipo = "0"
        id_tipo_perfil = "0"
        id_subtipo_perfil = "0"
        id_perfil = "0"
        id_tags = ""
    elseif  Request.QueryString("acao") = "alterar" AND Request.QueryString("id") <> "" then

        IF IsNumeric(Request.QueryString("id")) Then 

            title = "Alterar Perfil"
            ALTERAR = true
            id_perfil = LimparTexto(Request.QueryString("id"))

            WITH_ID = " AND tp.id_perfil = "& id_perfil

            set retorno_query = get_perfil(false, false, WITH_ID)
            Set perfil_categoria = get_perfil_categoria(id_perfil)
            Set perfil_tags = get_perfil_tags(id_perfil)

            If not retorno_query.EOF Then 'VERIFICA SE TRUE
                Do While Not retorno_query.EOF
                    id_perfil = retorno_query.Fields("id_perfil").Value
                    nm_perfil = retorno_query.Fields("nm_perfil").Value
                    email = retorno_query.Fields("nm_email").Value
                    dt_nascimento = FormataData(retorno_query.Fields("dt_nascimento").Value, 2)
                    ds_foto = retorno_query.Fields("ds_foto").Value
                    id_tipo_perfil = retorno_query.Fields("id_tipo").Value
                    id_subtipo_perfil = retorno_query.Fields("id_subtipo").Value
                retorno_query.MoveNext
                Loop
                retorno_query.close
            else 'SE NÃO ENCONTROU VOLTA PRA LISTAGEM'
                Response.Redirect REDIRECIONA
            end if
            'FINAL VERIFICA SE RETORNOU ALGO DO SELECT'
        else
            Response.Redirect REDIRECIONA
        end if
        'FINAL VERIFICA SE ID É UM NÚMERO'
    elseif  Request.QueryString("acao") = "visualizar" AND Request.QueryString("id") <> "" then
        IF IsNumeric(Request.QueryString("id")) Then     
            visualizar = true
            title = "Visualizar Perfil"
            id_perfil = LimparTexto(Request.QueryString("id"))

            WITH_ID = " AND tp.id_perfil = "& id_perfil
                                            'find, having, with_id
            set retorno_query = get_perfil(false, false, WITH_ID)

            If not retorno_query.EOF Then 'VERIFICA SE TRUE

                Do While Not retorno_query.EOF
                    id_perfil = retorno_query.Fields("id_perfil").Value
                    nm_perfil = retorno_query.Fields("nm_perfil").Value
                    email = retorno_query.Fields("nm_email").Value
                    dt_nascimento = FormataData(retorno_query.Fields("dt_nascimento").Value, 2)
                    ds_foto = retorno_query.Fields("ds_foto").Value
                    categorias = retorno_query.Fields("categorias").Value
                    nm_tipo = retorno_query.Fields("nm_tipo").Value
                    nm_subtipo = retorno_query.Fields("nm_subtipo").Value
                    tags = retorno_query.Fields("tags").Value
                    retorno_query.MoveNext
                Loop
                retorno_query.close
            else 'SE NÃO ENCONTROU VOLTA PRA LISTAGEM'
                Response.Redirect REDIRECIONA
            end if
            'FINAL VERIFICA SE RETORNOU ALGO DO SELECT'
        else
            Response.Redirect REDIRECIONA
        end if
        'FINAL VERIFICA SE ID É UM NÚMERO'
    else
        Response.Redirect REDIRECIONA
    end if
    'FINAL IF TIPO DE AÇÃO'
else
    Response.Redirect REDIRECIONA
end if
'FINAL IF Q IGUAL A S' 
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><%=title%></title>
    <!--#include file="include\css.asp"-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>

    <div id="wrapper">

     <!--#include file="include\menu.asp"-->

     <div id="page-wrapper">

        <div class="container-fluid">

            <h1><%=title%></h1>

            <div id="alert" style="display:none;"> </div>

            <form role="form" name="form_post_perfil" method="post" id="form_post_perfil" enctype="multipart/form-data">
                <% if visualizar = false then%>
                <label>Nome</label>
                <input type="text" required="required" placeholder="Nome" id="nm_perfil" name="nm_perfil" value="<%=nm_perfil%>"  maxlength="100"/>
                <br/><br/>
                <label>E-mail</label>
                <input type="email" autofocus maxlength="100" required="required" placeholder="seu@email.com" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="<%=email%>" />
                <br/><br/>
                <label>Data de nascimento</label>
                <input type="text" required="required" placeholder="DD/MM/YYYY" id="dt_nascimento" name="dt_nascimento" value="<%=dt_nascimento%>"  maxlength="10"/>
                <br/><br/>
                <label>Foto</label>
                <input type="file" id="foto" name="foto" ds-foto="<%=ds_foto%>" />          
                <% IF ALTERAR = TRUE Then %>
                <br/><br/>
                <img src="<%=RELATIVO_IMAGEM_PERFIL&id_perfil&pasta_thumb&ds_foto%>" alt="<%=nm_perfil%>" title="<%=nm_perfil%>" id="caminho_foto" />
                <% ENd if%>
                <br/><br/>
                <label>Categoria</label>
                <select data-placeholder="Sem categoria" name="id_categoria" id="id_categoria" class="chosen-select" multiple="multiple">
                    <option value=""></option>
                    <%  
                     If not categorias.EOF Then 'VERIFICA SE TRUE 
                            Do While Not categorias.EOF

                            pk_categoria = categorias.Fields("id_categoria").value
                            nm_categoria = categorias.Fields("nm_categoria").value
                                
                                response.write("<option value='"&pk_categoria&"' ")
                                
                                IF ALTERAR = TRUE then
                                    if not perfil_categoria.EOF then
                                        id_perfil_categoria = ""
                                        Do While not perfil_categoria.EOF
                                           'Response.write(perfil_categoria.Fields("id_categoria").Value & " > ")
                                           id_perfil_categoria = perfil_categoria.Fields("id_categoria").value
                                           if pk_categoria = id_perfil_categoria then 
                                                response.write("selected='selected'")
                                            end if
                                            'FINAL VERICIA SE ID SELECIONADO É IGUAL ALGUM DO BANCO'
                                            perfil_categoria.MoveNext
                                        Loop
                                        'FINAL LOOP WHILE DE PERFIL CATEGORIA' 
                                        perfil_categoria.MoveFirst                          
                                    end if
                                    'FINAL VERIFICA PERFIL_CATEGORIA'
                                end if
                                'FINAL IF ALTERAR IGUAL TRUE'
                            categorias.MoveNext
                            response.write(" >"&nm_categoria&"</option>")
                            Loop
                            'FINAL LOOP WHILE DE CATEGORIAS'

                            IF ALTERAR = TRUE then 
                                perfil_categoria.close 
                            end if
                            categorias.close
                        end If
                        'FINAL _GET CATEGORIAS'
                            %>
                        </select>
                        <br/><br/>
                         <label>Tipo</label>
                         <select name="id_tipo" id="id_tipo">
                            <option value="">Selecione um tipo</option>
                            <%  
                            If not tipos.EOF Then 'VERIFICA SE TRUE 
                                Do While Not tipos.EOF

                                pk_tipo = tipos.Fields("id_tipo").Value
                                nm_tipo = tipos.Fields("nm_tipo").Value
                                if pk_tipo = id_tipo_perfil then 
                                    selecionado = "selected=""selected"""
                                else
                                    selecionado = ""
                                end if
                                'FINAL VERICIA SE ID SELECIONADO É IGUAL ALGUM DO BANCO'
                                %>
                                <option value="<%=pk_tipo%>" <%=selecionado%>><%=nm_tipo%></option>
                                <%
                                tipos.MoveNext
                                Loop
                                'FINAL LOOP WHILE'
                                tipos.close
                                end If
                            'FINAL _GET TIPOS'
                            %>
                         </select>
                        <br/><br/>
                        <label>Subtipo</label>
                        <select name="id_subtipo" id="id_subtipo" data-id="<%=id_subtipo_perfil%>">
                            <option value="">Selecione tipo</option>
                        </select>
                        <br/><br/>
                        <label>Tags</label>
                        <input type="text" name="tags[]" value="" class="tags" id="tags"/>
                            <%  
                              If not tags.EOF Then 'VERIFICA SE TRUE 
                                Do While Not tags.EOF
                                   pk_tags = tags.Fields("id_tags").Value
                                   nm_tags = tags.Fields("nm_tags").Value

                                    IF ALTERAR = TRUE then
                                        if not perfil_tags.EOF then
                                            Do While not perfil_tags.EOF
                                               id_perfil_tags = perfil_tags.Fields("id_tags").Value
                                               if pk_tags = id_perfil_tags then 
                                                    response.write("<input type='hidden' name='tags["& pk_tags &"-a]' value='" & nm_tags & "' class='tags' id='tags' />")
                                                end if
                                                'FINAL VERICIA SE ID SELECIONADO É IGUAL ALGUM DO BANCO'
                                              perfil_tags.MoveNext
                                            Loop
                                            'FINAL LOOP do WHILE PERFIL TAGS'
                                            perfil_tags.MoveFirst    
                                        end if
                                        'FINAL VERIFICA PERFIL_TAGS'
                                    end if
                                    'FINAL VERIFICA SE ALTERAR IGUAL A TRUE'                              
                                    tags.MoveNext
                                Loop
                                'FINAL LOOP WHILE'
                                IF ALTERAR = TRUE then
                                    perfil_tags.close
                                end if
                                    tags.close
                                end If
                                'FINAL _GET tags'
                            %>

                        <br/><br/>
                        <label>Captcha</label>
                         <% if server_response <> "" or newCaptcha then %>

                            <% if newCaptcha = False then %>
                             <!-- An error occurred -->
                              Wrong!

                            <% end if %>
                        <div class="g-recaptcha" data-sitekey="6LcwKQcTAAAAAB2OFO7rDvX12ZbDRDsX9ufaT_Ni"></div>
                         <% else %>

                            <!-- The solution was correct -->
                            Correct!

                          <%end if%>
                        <br/><br/>
                        <input type="hidden" id="id_perfil" name="id_perfil" value="<%=id_perfil%>" />
                        <input type="submit" name="Salvar" id="btnSalvar" value="Salvar"/>
                        <% else %>
                            <label>Nome: <%=nm_perfil%></label>
                            <br/><br/>
                            <label>E-mail: <%=email%></label>
                            <br/><br/>
                            <label>Data de nascimento : <%=dt_nascimento%></label>
                            <br/><br/>
                            <label>Foto: <img src="<%=RELATIVO_IMAGEM_PERFIL&id_perfil&pasta_thumb&ds_foto%>" alt="<%=nm_perfil%>" title="<%=nm_perfil%>" id="caminho_foto" /></label>
                            <br/><br/>
                            <label>Categoria: <%=categorias%></label>
                            <br/><br/>
                            <label>Tipo: <%=nm_tipo%></label>
                            <br/><br/>
                            <label>Subtipo: <%=nm_subtipo%></label>
                            <br/><br/>
                            <label>Tags: <%=tags%></label>
                        <% end if%>

                    </form>
                    <div class="" id="preview_imagem">
                        <input type="button" id="btnCrop" value="Crop" style="display: none" />
                        <br/>
                        <img id="Image1" src="" alt="" style="display: none" />
                        <canvas id="canvas" height="5" width="5"></canvas>
                        <br />                        
                        <input type="hidden" name="imgX1" id="imgX1" />
                        <input type="hidden" name="imgY1" id="imgY1" />
                        <input type="hidden" name="imgWidth" id="imgWidth" />
                        <input type="hidden" name="imgHeight" id="imgHeight" />
                        <input type="hidden" name="imgCropped" id="imgCropped" />
                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <!--#include file="include\js.asp"-->
        <script src="assets/js/perfil.js"></script>
    </body>

    </html>
